const mongoose = require('mongoose')

const Tasks = mongoose.model('Tasks', 
{
    Owner: {
        type: String,
        required: true,
    },
    Description: {
        type: String,
        required: true,
        trim: true,
        validate(value) {
            if (value.length <= 0) {
                throw new Error('No description please describe the task')
            }
        }
    },
    isCompleted: {
        type: Boolean,
        default: false,
    }

})