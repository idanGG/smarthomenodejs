const express =  require('express')
const taskRouter = require('./router/taskRouter')

const app = express()
const port = process.env.PORT || 3000

// Using this line of code to parse incoming data into json
app.use(express.json())
app.use(taskRouter)

app.listen(port, () => {
    console.log('Server is up on port' +  port);
})
